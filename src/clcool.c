#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "clcool.h"

#define N_CLCOOL_RED_BIN 30
#define N_CLCOOL_MET_BIN 8
#define N_CLCOOL_DEN_BIN 13
#define N_CLCOOL_TEM_BIN 33 
#define N_CLCOOL_VAR     5 /* 0: log10(cooling rate)
                              1: log10(heating rate)
                              2: log10(u erg/g)
                              3: mean molecular weight
                              4: log10(ne /cm^3)
                              */

#define Log_T_min 1
#define Log_T_max 9
#define D_Log_T   0.25

#define Log_nH_min -6
#define Log_nH_max 6
#define D_Log_nH   1

#define CL_NAME_BASE "rahmati"

static double Log_nH_CR = -2.0; 

static double cool_redshift_table[N_CLCOOL_RED_BIN]  = {0, 0.25, 0.5, 0.75, 1.0, 1.25, 
  1.5, 1.75, 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.5, 5.0, 5.5, 6, 7, 8, 
  9, 10, 11, 12, 13, 14, 15};  
static double cool_metal_table[N_CLCOOL_MET_BIN] = {-10, -3, -2, -1, -0.5, 0, 0.5, 1}; 
static double cool_nH_table[N_CLCOOL_DEN_BIN]; 
static double cool_temp_table[N_CLCOOL_TEM_BIN]; 

static double clcool_table[N_CLCOOL_RED_BIN + 1][N_CLCOOL_MET_BIN][N_CLCOOL_DEN_BIN][N_CLCOOL_TEM_BIN][N_CLCOOL_VAR]; 

static void calc_density_bin(double *nH_table);   
static void calc_temperature_bin(double *temp_table); 
static void cl_read_tables(char *data_dir); 
static int cl_binary_search(double *table, int table_size, double value); 

static double clcool_table_z[N_CLCOOL_MET_BIN][N_CLCOOL_DEN_BIN][N_CLCOOL_TEM_BIN][N_CLCOOL_VAR]; 
static double interp_table[N_CLCOOL_TEM_BIN][N_CLCOOL_VAR]; 

void init_clcool_tables(char *data_dir) 
{
  calc_density_bin(cool_nH_table); 
  calc_temperature_bin(cool_temp_table); 
  cl_read_tables(data_dir); 
}

double cl_find_u(double log_T) 
{
  double log_u; 
  int itemp;   
  double var1, var2, x1, x2, dx; 
  if (log_T < Log_T_min) {
    log_u = interp_table[0][2]; 
  } else if (log_T >= Log_T_max) {
    log_u = interp_table[N_CLCOOL_TEM_BIN - 1][2]; 
  } else {
    itemp = (log_T - Log_T_min)/D_Log_T; 
    x1 = Log_T_min + D_Log_T*itemp; 
    x2 = Log_T_min + D_Log_T*(itemp + 1); 
    dx = log_T - x1; 
    var1 = interp_table[itemp][2]; 
    var2 = interp_table[itemp + 1][2]; 
    log_u = var1 + ((var2 - var1)/(x2 - x1)) * dx;  
  }
  return pow(1.e1, log_u); 
}
   
/* calculate temperature from specific internal energy */
double cl_find_logT_from_u(double uspec) 
{
  int itemp; 
  double log_T, log_u; 
  double cool_uspec_table[N_CLCOOL_TEM_BIN]; 
  double var1, var2, x1, x2, dx; 
  
  for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
    cool_uspec_table[itemp] = interp_table[itemp][2]; 
  }

  if (uspec >= pow(1.e1, cool_uspec_table[N_CLCOOL_TEM_BIN - 1]) ) {
    itemp = N_CLCOOL_TEM_BIN - 1; 
    log_T = cool_temp_table[itemp]; 
  } else if (uspec < pow(1.e1, cool_uspec_table[0])) {
    itemp = 0; 
    log_T = cool_temp_table[itemp]; 
  } else {
    log_u = log10(uspec); 
    itemp = cl_binary_search(cool_uspec_table, N_CLCOOL_TEM_BIN, log_u); 
    var1 = cool_temp_table[itemp]; 
    var2 = cool_temp_table[itemp + 1]; 
    x1 = cool_uspec_table[itemp]; 
    x2 = cool_uspec_table[itemp + 1]; 
    dx = log_u - x1;  
    log_T = var1 + ((var2 - var1)/(x2 - x1))*dx; 
  }
  return log_T; 
}

double cl_find_mu(double log_T) 
{
  double mu; 
  int itemp;   
  double var1, var2, x1, x2, dx; 
  if (log_T < Log_T_min) {
    mu = interp_table[0][3]; 
  } else if (log_T >= Log_T_max) {
    mu = interp_table[N_CLCOOL_TEM_BIN - 1][3]; 
  } else {
    itemp = (log_T - Log_T_min)/D_Log_T; 
    x1 = Log_T_min + D_Log_T * itemp; 
    x2 = Log_T_min + D_Log_T * (itemp + 1); 
    dx = log_T - x1; 
    var1 = interp_table[itemp][3]; 
    var2 = interp_table[itemp + 1][3]; 
    mu = var1 + ((var2 - var1)/(x2 - x1)) * dx;  
  }
  return mu; 
}

double cl_find_log_ne(double log_T) 
{
  double log_ne; 
  int itemp;   
  double var1, var2, x1, x2, dx; 
  if (log_T < Log_T_min) {
    log_ne = interp_table[0][4]; 
  } else if (log_T >= Log_T_max) {
    log_ne = interp_table[N_CLCOOL_TEM_BIN - 1][4]; 
  } else {
    itemp = (log_T - Log_T_min)/D_Log_T; 
    x1 = Log_T_min + D_Log_T * itemp; 
    x2 = Log_T_min + D_Log_T * (itemp + 1); 
    dx = log_T - x1; 
    var1 = interp_table[itemp][4]; 
    var2 = interp_table[itemp + 1][4]; 
    log_ne = var1 + ((var2 - var1)/(x2 - x1)) * dx;  
  }
  return log_ne; 
}

/* return cooling rate */
double cl_cooling_rate(double log_T, double log_nH, double zred, double z_reion) 
{
  int itemp; 
  double var1, var2, x1, x2, dx; 
  double log_lambda, lambda; 
  double ne, T, nH; 
  
  if (log_T < Log_T_min) {
    log_lambda = interp_table[0][0]; 
  } else if (log_T >= Log_T_max) {
    log_lambda = interp_table[N_CLCOOL_TEM_BIN - 1][0]; 
  } else {
    itemp = (log_T - Log_T_min)/D_Log_T; 
    x1 = Log_T_min + itemp * D_Log_T; 
    x2 = Log_T_min + (itemp + 1) * D_Log_T; 
    dx = log_T - x1; 
    var1 = interp_table[itemp][0]; 
    var2 = interp_table[itemp + 1][0]; 
    log_lambda = var1 + ((var2 - var1)/(x2 - x1)) * dx;  
  }

  lambda = pow(1.e1, log_lambda); 

  if (zred > z_reion) {
    /* inverse Compton cooling */ 
    ne = pow(1.e1, cl_find_log_ne(log_T)); 
    T  = pow(1.e1, log_T); 
    nH = pow(1.e1, log_nH); 
    lambda += 5.65e-36 * pow(1.0 + zred, 4.0) * (T - 2.728 * (1.0 + zred)) * ne/nH; 
  }
  
  return lambda; 
}

/* returns heating rate */
double cl_heating_rate(double log_T) 
{  
  int itemp; 
  double var1, var2, x1, x2, dx; 
  double log_heat; 
  
  if (log_T < Log_T_min) {
    log_heat = interp_table[0][1]; 
  } else if (log_T >= Log_T_max) {
    log_heat = interp_table[N_CLCOOL_TEM_BIN - 1][1]; 
  } else {
    itemp = (log_T - Log_T_min)/D_Log_T; 
    x1 = Log_T_min + itemp * D_Log_T; 
    x2 = Log_T_min + (itemp + 1) * D_Log_T; 
    dx = log_T - x1; 
    var1 = interp_table[itemp][1]; 
    var2 = interp_table[itemp + 1][1]; 
    log_heat = var1 + ((var2 - var1)/(x2 - x1)) * dx;  
  }
  return pow(1.e1, log_heat);  
}

/* heating - cooling */
double cl_heatcoolrate(double log_T, double log_nH, double zred, double z_reion) 
{
  int itemp; 
  double var1, var2, x1, x2, dx; 
  double log_lambda, log_heat; 
  double lambda, ne, T, nH;  

  
  if (log_T < Log_T_min) {
    log_lambda = interp_table[0][0]; 
    log_heat   = interp_table[0][1]; 
  } else if (log_T >= Log_T_max) {
    log_lambda = interp_table[N_CLCOOL_TEM_BIN - 1][0]; 
    log_heat   = interp_table[N_CLCOOL_TEM_BIN - 1][1]; 
  } else {
    itemp = (log_T - Log_T_min)/D_Log_T; 
    x1 = Log_T_min + itemp * D_Log_T; 
    x2 = Log_T_min + (itemp + 1) * D_Log_T; 
    dx = log_T - x1; 
    var1 = interp_table[itemp][0]; 
    var2 = interp_table[itemp + 1][0]; 
    log_lambda = var1 + ((var2 - var1)/(x2 - x1)) * dx;  
    var1 = interp_table[itemp][1]; 
    var2 = interp_table[itemp + 1][1]; 
    log_heat = var1 + ((var2 - var1)/(x2 - x1)) * dx;  
  }
  lambda = pow(1.e1, log_lambda); 

  if (zred > z_reion) { 
    /* inverse Compton cooling */ 
    ne = pow(1.e1, cl_find_log_ne(log_T)); 
    T  = pow(1.e1, log_T); 
    nH = pow(1.e1, log_nH); 
    lambda += 5.65e-36 * pow(1.0 + zred, 4.0) * (T - 2.728 * (1.0 + zred)) * ne/nH; 
  }
  return pow(1.e1, log_heat) - lambda; 
  //return pow(1.e1, log_lambda); 
}

static void calc_density_bin(double *nH_table) 
{
  int i; 
  for (i = 0; i < N_CLCOOL_DEN_BIN; ++i) 
    nH_table[i] = Log_nH_min + D_Log_nH * i; 
}


static void calc_temperature_bin(double *temp_table) 
{

  int i; 
  for (i = 0; i < N_CLCOOL_TEM_BIN; ++i) 
    temp_table[i] = Log_T_min + D_Log_T * i; 
}

static void cl_read_tables(char *data_dir)
{ 
  char buf[256]; 
  int iz, imet, iden; 
  double redshift; 
  size_t read_size; 
  FILE *fd; 

  for (iz = 0; iz < N_CLCOOL_RED_BIN; ++iz) {
    redshift = cool_redshift_table[iz]; 
    for (imet = 0; imet < N_CLCOOL_MET_BIN; ++imet) { 
      for (iden = 0; iden < N_CLCOOL_DEN_BIN; ++iden) {
        sprintf(buf, "%s/%s_z%02d.%02d_Z%2.2d_nH%2.2d.dat", 
            data_dir, CL_NAME_BASE, (int)redshift, (int)(100*(redshift - (int)redshift)), imet, iden);  
    
        if(!(fd = fopen(buf,      "rb"))) {
          fprintf(stderr,"The file %s cannot be opened.\n", buf); 
          exit(3); 
        }
    
        read_size = fread(clcool_table[iz][imet][iden], sizeof(double), N_CLCOOL_TEM_BIN*N_CLCOOL_VAR, fd); 
        fclose(fd); 
      }
    }
  }

  /* CIE table */
  for (imet = 0; imet < N_CLCOOL_MET_BIN; ++imet) { 
    for (iden = 0; iden < N_CLCOOL_DEN_BIN; ++iden) { 
      sprintf(buf, "%s/%s_CIE_Z%2.2d_nH%2.2d.dat", data_dir, CL_NAME_BASE, imet, iden); 
      if(!(fd = fopen(buf,      "rb"))) { 
        fprintf(stderr,"The file %s cannot be opened.\n", buf); 
        exit(3); 
      } 
      read_size = fread(clcool_table[N_CLCOOL_RED_BIN][imet][iden], sizeof(double), N_CLCOOL_TEM_BIN*N_CLCOOL_VAR, fd); 
      fclose(fd); 
    }
  }
}

// this returs the index of in the redshift bin
int cl_load_redshift_tables(double zred, double z_reion) 
{
  int imet, inh, itemp, ivar; 
  static int izred = -1;  
  double var1, var2, x1, x2, dx; 

  if (zred > z_reion) {
    if (izred == N_CLCOOL_RED_BIN) 
      return izred; 
    for (imet = 0; imet < N_CLCOOL_MET_BIN; ++imet) {
      for (inh = 0; inh < N_CLCOOL_DEN_BIN; ++inh) {
        for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
          for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) {
            clcool_table_z[imet][inh][itemp][ivar] 
              = clcool_table[N_CLCOOL_RED_BIN][imet][inh][itemp][ivar]; 
          }
        }
      }
    }
    izred = N_CLCOOL_RED_BIN; 
    return izred; 
  } else { 
    if (izred < 0 || izred == N_CLCOOL_RED_BIN)  
      izred = cl_binary_search(cool_redshift_table, N_CLCOOL_RED_BIN, zred); 
    else if (izred < N_CLCOOL_RED_BIN - 1) {
      if (cool_redshift_table[izred] > zred || zred >= cool_redshift_table[izred + 1]) {
        izred = cl_binary_search(cool_redshift_table, N_CLCOOL_RED_BIN, zred); 
      }
    }

    for (imet = 0; imet < N_CLCOOL_MET_BIN; ++imet) {
      for (inh = 0; inh < N_CLCOOL_DEN_BIN; ++inh) {
        for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
          for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) {
            var1 = clcool_table[izred][imet][inh][itemp][ivar]; 
            var2 = clcool_table[izred + 1][imet][inh][itemp][ivar]; 
            x1 = cool_redshift_table[izred]; 
            x2 = cool_redshift_table[izred + 1]; 
            dx = zred - x1;             
            clcool_table_z[imet][inh][itemp][ivar] = var1 + ((var2 - var1)/(x2 - x1)) * dx; 
          }
        }
      }
    }
    return izred; 
  }
}

static int cl_binary_search(double *table, int table_size, double value) 
{
  int first, last, middle; 
      
  first = 0; 
  last = table_size - 1; 
  middle = (first + last) / 2; 


  while (first < middle) {
    /*
    printf("first = %d middle = %d\n", first, middle); 
    */
    if (table[middle] < value) 
      first = middle; 
    else if (table[middle] > value) 
      last = middle; 
    else 
      return middle; 

    middle = (first + last) / 2; 
  }
  return middle;  
}

void cl_interp_tables(double Metallicity, double log_nH) 
{
  int imet, inh, itemp, ivar; 
  double Z; 
  double tmp_table[N_CLCOOL_DEN_BIN][N_CLCOOL_TEM_BIN][N_CLCOOL_VAR]; 
  double var1, var2; 
  double x1, x2, dx; 

  if (Metallicity/CL_Z_SUN < pow(1.e1, cool_metal_table[0])) 
    Z = pow(1.e1, cool_metal_table[0]); 
  else 
    Z = Metallicity/CL_Z_SUN; 
  
  /* interpolate in metallicity */
  if (Z >= pow(1.e1, cool_metal_table[N_CLCOOL_MET_BIN - 1])) {
    for (inh = 0; inh < N_CLCOOL_DEN_BIN; ++inh) {
      for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
        for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
          tmp_table[inh][itemp][ivar] =  
            clcool_table_z[N_CLCOOL_MET_BIN - 1][inh][itemp][ivar]; 
        }
      }
    }
  } else if (Z >= pow(1.e1, cool_metal_table[0])) {
    imet = cl_binary_search(cool_metal_table, N_CLCOOL_MET_BIN, log10(Z)); 
    x1 = pow(1.e1, cool_metal_table[imet]); 
    x2 = pow(1.e1, cool_metal_table[imet + 1]); 
    dx = Z - x1; 
    for (inh = 0; inh < N_CLCOOL_DEN_BIN; ++inh) {
      for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
        for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
          var1 = clcool_table_z[imet][inh][itemp][ivar]; 
          var2 = clcool_table_z[imet + 1][inh][itemp][ivar]; 
          tmp_table[inh][itemp][ivar] 
            = var1 + ((var2 - var1)/(x2 - x1)) * dx;   
        }
      }
    }
  }

  /* interpolate in density */
  if (log_nH < cool_nH_table[0]) {
    for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
      for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
        interp_table[itemp][ivar] = tmp_table[0][itemp][ivar];  
      }
    }
  } else if (log_nH >= cool_nH_table[N_CLCOOL_DEN_BIN - 1]) {
    for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
      for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
        interp_table[itemp][ivar] = tmp_table[N_CLCOOL_DEN_BIN - 1][itemp][ivar];  
      }
    }
  } else {
    inh = cl_binary_search(cool_nH_table, N_CLCOOL_DEN_BIN, log_nH);  
    x1 = cool_nH_table[inh]; 
    x2 = cool_nH_table[inh + 1]; 
    dx = log_nH - x1; 
    for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
      for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
        var1 = tmp_table[inh][itemp][ivar]; 
        var2 = tmp_table[inh + 1][itemp][ivar]; 
        interp_table[itemp][ivar] = var1 + ((var2 - var1)/(x2 - x1)) * dx; 
      }
    }
  }
}

#ifdef __CL_TEST_COOL__
void cl_interp_metal_log(double Metallicity, double log_nH) 
{
  int imet1, imet2, inh, itemp, ivar; 
  double log_Z; 
  double tmp_table[N_CLCOOL_DEN_BIN][N_CLCOOL_TEM_BIN][N_CLCOOL_VAR]; 
  double var1, var2; 
  double x1, x2, dx; 

  if (Metallicity/CL_Z_SUN < pow(1.e1, cool_metal_table[0])) 
    log_Z = cool_metal_table[0]; 
  else 
    log_Z = log10(Metallicity/CL_Z_SUN); 

  printf("log10(Z/Zsun) = %e\n", log_Z); 


  /* interpolate in metallicity in log scale by using Z = 1.e-10 Zsun and Z = Zsun */
  imet1 = 0; 
  imet2 = 5; 
  x1 = cool_metal_table[imet1]; 
  x2 = cool_metal_table[imet2]; 
  dx = log_Z - x1; 
  for (inh = 0; inh < N_CLCOOL_DEN_BIN; ++inh) {
    for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
      for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
          var1 = clcool_table_z[imet1][inh][itemp][ivar]; 
          var2 = clcool_table_z[imet2][inh][itemp][ivar]; 
          tmp_table[inh][itemp][ivar] 
            = var1 + ((var2 - var1)/(x2 - x1)) * dx;   
      }
    }
  }  
  
  /* interpolate in density */
  if (log_nH < cool_nH_table[0]) {
    for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
      for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
        interp_table[itemp][ivar] = tmp_table[0][itemp][ivar];  
      }
    }
  } else if (log_nH >= cool_nH_table[N_CLCOOL_DEN_BIN - 1]) {
    for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
      for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
        interp_table[itemp][ivar] = tmp_table[N_CLCOOL_DEN_BIN - 1][itemp][ivar];  
      }
    }
  } else {
    inh = cl_binary_search(cool_nH_table, N_CLCOOL_DEN_BIN, log_nH);  
    x1 = cool_nH_table[inh]; 
    x2 = cool_nH_table[inh + 1]; 
    dx = log_nH - x1; 
    for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
      for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
        var1 = tmp_table[inh][itemp][ivar]; 
        var2 = tmp_table[inh + 1][itemp][ivar]; 
        interp_table[itemp][ivar] = var1 + ((var2 - var1)/(x2 - x1)) * dx; 
      }
    }
  }

}

void cl_interp_metal_linear(double Metallicity, double log_nH)
{
  int imet1, imet2, inh, itemp, ivar; 
  double Z; 
  double tmp_table[N_CLCOOL_DEN_BIN][N_CLCOOL_TEM_BIN][N_CLCOOL_VAR]; 
  double var1, var2; 
  double x1, x2, dx; 
  if (Metallicity/CL_Z_SUN < pow(1.e1, cool_metal_table[0])) 
    Z = pow(1.e1, cool_metal_table[0]); 
  else 
    Z = Metallicity/CL_Z_SUN; 
  printf("log10(Z/Zsun) = %e\n", log10(Z)); 

  /* interpolate in metallicity in linear scale by using Z = 1.e-10 Zsun and Z = Zsun */
  imet1 = 0; 
  imet2 = 5; 
  x1 = pow(1.e1, cool_metal_table[imet1]); 
  x2 = pow(1.e1, cool_metal_table[imet2]); 
  dx = Z -  x1; 
  for (inh = 0; inh < N_CLCOOL_DEN_BIN; ++inh) {
    for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
      for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
          var1 = clcool_table_z[imet1][inh][itemp][ivar]; 
          var2 = clcool_table_z[imet2][inh][itemp][ivar]; 
          tmp_table[inh][itemp][ivar] 
            = var1 + ((var2 - var1)/(x2 - x1)) * dx;   
      }
    }
  }  

  /* interpolate in density */
  if (log_nH < cool_nH_table[0]) {
    for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
      for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
        interp_table[itemp][ivar] = tmp_table[0][itemp][ivar];  
      }
    }
  } else if (log_nH >= cool_nH_table[N_CLCOOL_DEN_BIN - 1]) {
    for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
      for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
        interp_table[itemp][ivar] = tmp_table[N_CLCOOL_DEN_BIN - 1][itemp][ivar];  
      }
    }
  } else {
    inh = cl_binary_search(cool_nH_table, N_CLCOOL_DEN_BIN, log_nH);  
    x1 = cool_nH_table[inh]; 
    x2 = cool_nH_table[inh + 1]; 
    dx = log_nH - x1; 
    for (itemp = 0; itemp < N_CLCOOL_TEM_BIN; ++itemp) {
      for (ivar = 0; ivar < N_CLCOOL_VAR; ++ivar) { 
        var1 = tmp_table[inh][itemp][ivar]; 
        var2 = tmp_table[inh + 1][itemp][ivar]; 
        interp_table[itemp][ivar] = var1 + ((var2 - var1)/(x2 - x1)) * dx; 
      }
    }
  }
}

#endif 
