#ifndef __CLCOOL_H__
#define __CLCOOL_H__

#define CL_Z_SUN 0.0129564941552

void init_clcool_tables(char *data_dir); 
/* call this function before calling other loutines at each redshift */
int cl_load_redshift_tables(double redshift, double z_reion); 
/*
   create an interpolated table. Variables are now functions of temperature. 
   call this function when once for a new combinagtion of (Metallicity, log_nH) 
   */ 
void cl_interp_tables(double Metallicity, double log_nH);  

/* give the internal specific energy, then it retruns log10(T/K) */
double cl_find_logT_from_u(double uspec);  
/* give the temperature, then it retruns log10(ne/cm^3) */
double cl_find_log_ne(double log_T);  
double cl_find_mu(double log_T);  
double cl_find_u(double log_T); 
/* feed redshift and z_reion to compute the inverse Compton cooling for 
   z > z_reion */
double cl_cooling_rate(double log_T, double log_nH, double redshift, double z_reion);  
double cl_heating_rate(double log_T);  
/* heating - cooling */
double cl_heatcoolrate(double log_T, double log_nH, double redshift, double z_reion); 

#ifdef __CL_TEST_COOL__
void cl_interp_metal_log(double Metallicity, double log_nH);  
void cl_interp_metal_linear(double Metallicity, double log_nH); 
#endif 
#endif
