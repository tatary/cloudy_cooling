#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "clcool.h"

typedef struct RunParams {
  char data_dir[256]; 
  double z_reion; 
  double redshift; 
  double metallicity;  
  double nH; 
  char out_base[256]; 
} run_params; 

static void cl_read_parameters(char *file_name, run_params *this_run); 
static void calc_cooling_table(char *out_base, double redshift, double metallicity, double nH); 
static void compare_coolfunc(double log_nH, double zred, double z_reion); 

static void write_coolheat_table(char *out_base, double z_reion, double redshift, 
    double metallicity, double nH); 

int main(int argc, char **argv) {

  char parameter_file[256]; 
  run_params this_run; 

  if (argc < 2) {
    fprintf(stderr, "you have to give the name of the parameter file\n"); 
    exit(1); 
  }
  strcpy(parameter_file, argv[1]); 

  cl_read_parameters(parameter_file, &this_run); 

  init_clcool_tables(this_run.data_dir); 
#ifndef __CL_COOL_TEST
  write_coolheat_table(this_run.out_base, this_run.z_reion, 
      this_run.redshift, this_run.metallicity, this_run.nH); 
#else 
  compare_coolfunc(log10(this_run.nH), this_run.redshift, this_run.z_reion); 
#endif

  return 0; 
}

/*
   read the data dir and reionization redshift from the paramter file 
   */
static void cl_read_parameters(char *file_name, run_params *this_run) 
{
  FILE *fd; 
  char buf[256], buf1[256], buf2[256]; 


  if(!(fd = fopen(file_name, "r"))) {
    fprintf(stderr, "The file %s cannot be opened.\n", file_name); 
    exit(2); 
  }

  while (!feof(fd)) {
    *buf = 0; 
    if (fgets(buf, 256, fd)) { 
      sscanf(buf, "%s%s", buf1, buf2); 
      if (buf[0] == '#') 
        continue; 
      if (strcmp(buf1, "data_dir")==0) {
        strcpy(this_run->data_dir, buf2); 
      }
      if (strcmp(buf1, "z_reion")==0) {
        this_run->z_reion =  atof(buf2); 
      }
      if (strcmp(buf1, "redshift")==0) {
        this_run->redshift = atof(buf2); 
      }
      if (strcmp(buf1, "metallicity")==0) {
        this_run->metallicity = atof(buf2); 
      }
      if (strcmp(buf1, "nH")==0) {
        this_run->nH = atof(buf2); 
      }
      if (strcmp(buf1, "out_base")==0) {
        strcpy(this_run->out_base, buf2); 
      }
    }
  }
  fclose(fd); 
}

static void write_coolheat_table(char *out_base, double z_reion, double redshift, 
    double metallicity, double nH) 
{
  FILE *fd; 
  char buf[256]; 
  int itemp, ntemp; 
  int izred; 
  double log_T_min = 1; 
  double log_T_max = 9; 
  double D_log_T = 0.1; 
  double log_T, heatcool; 

  izred = cl_load_redshift_tables(redshift, z_reion); 
  printf("izred = %d\n", izred); 

  cl_interp_tables(metallicity, log10(nH)); 

  ntemp = (log_T_max - log_T_min) / D_log_T; 
  sprintf(buf, "%s.txt", out_base); 
  fd = fopen(buf, "w");  
 // for (itemp = 0; itemp < ntemp; ++itemp) 

  cl_interp_tables(0.0, 1.753527e-01); 
  printf("rate = %e\n", cl_heatcoolrate(2.581448, 1.753527e-01, 0.0, 8)); 
  cl_interp_tables(metallicity, log10(nH)); 
  for (itemp = 0; itemp < 33; ++itemp) 
  {
    //log_T = log_T_min + itemp * D_log_T; 
    log_T = log_T_min + itemp * 0.25; 
    heatcool = cl_heatcoolrate(log_T, log10(nH), redshift, z_reion); 
    fprintf(fd, "%e %e %e %e %e %e\n", log_T, log10(fabs(heatcool)), 
        log10(fabs(cl_cooling_rate(log_T, log10(nH), redshift, z_reion))), 
        log10(fabs(cl_heating_rate(log_T))), log10(cl_find_u(log_T)), heatcool); 
  }
  fclose(fd); 
}

#ifdef __CL_COOL_TEST__

static void compare_coolfunc(double log_nH, double zred, double z_reion) 
{
#define NTEMP 33 
  double Log_T_min = 1; 
  double Log_T_max = 9; 
  double dLogT     = 0.25; 
  double log_T; 
  int izred, imet, itemp;  
  double metal_table[3] = {1.e-2, 1.e-1, 0.2}; 
  double clrate[NTEMP], interp_log_rate[NTEMP], interp_linear_rate[NTEMP];  
  FILE *fl; 
  char fname[256]; 

  izred = cl_load_redshift_tables(zred, z_reion); 
  printf("izred = %d\n", izred); 

  for (imet = 0; imet < 3; ++imet) {

    cl_interp_tables(metal_table[imet]*CL_Z_SUN, log_nH); 
    for (itemp = 0; itemp < NTEMP; ++itemp) {
      log_T = Log_T_min + itemp * dLogT; 
      clrate[itemp] = cl_heatcoolrate(log_T, log_nH, zred, z_reion); 
    }
    cl_interp_metal_log(metal_table[imet]*CL_Z_SUN, log_nH);  
    for (itemp = 0; itemp < NTEMP; ++itemp) {
      log_T = Log_T_min + itemp * dLogT; 
      interp_log_rate[itemp] = cl_heatcoolrate(log_T, log_nH, zred, z_reion); 
    }
    cl_interp_metal_linear(metal_table[imet]*CL_Z_SUN, log_nH);  
    for (itemp = 0; itemp < NTEMP; ++itemp) {
      log_T = Log_T_min + itemp * dLogT; 
      interp_linear_rate[itemp] = cl_heatcoolrate(log_T, log_nH, zred, z_reion); 
    }

    sprintf(fname, "cool_comp_%03d.txt", (int)(100 * metal_table[imet])); 
    fl = fopen(fname, "w"); 
    for (itemp = 0; itemp < NTEMP; ++itemp) {
      log_T = Log_T_min + itemp * dLogT; 
      fprintf(fl, "%e %e %e %e %e %e\n", log_T, log10(fabs(clrate[itemp])), log10(fabs(interp_log_rate[itemp])), 
          log10(fabs(interp_linear_rate[itemp])), 
          fabs((interp_log_rate[itemp] - clrate[itemp])/clrate[itemp]), 
          fabs((interp_linear_rate[itemp] - clrate[itemp])/clrate[itemp])); 
    }
    fclose(fl); 
  }
}
#endif
