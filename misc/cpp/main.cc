#include "run_params.h"

#include <iostream>
#include <stdexcept>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <fstream>

#include <cstdlib>
#include <cstdio>
#include <cmath>

#include <boost/format.hpp>

#include "run_params.h"
#include "read_params.h"
#include "utils.h"

using std::cout; 
using std::endl; 
using std::cerr; 
using std::string; 
using std::vector; 
using std::pair; 
using std::ostringstream; 


int main(int argc, char **argv) 
{
  try {
    RunParams this_run; 

    string param_file_name; 
    if(argc == 1) {
      param_file_name = string("./param.txt"); 
    } else {
      param_file_name = string(argv[1]); 
    }

    read_params(this_run, param_file_name); 

    this_run.n_grid = this_run.redshifts.size() * this_run.iden_array.size(); 

    vector<pair<double, int> > param_pairs; 
    for (vector<double>::iterator ziter = this_run.redshifts.begin(); ziter != this_run.redshifts.end(); ++ziter) {
      for (vector<int>::iterator niter = this_run.iden_array.begin(); niter != this_run.iden_array.end(); ++niter) {
        pair<double, int> tmp_pair; 
        tmp_pair.first = *ziter; 
        tmp_pair.second = *niter; 
        param_pairs.push_back(tmp_pair); 
      }
    }

    // OK, let's run cloudy.exe 

    const int roop_size = param_pairs.size(); 
#pragma omp parallel for schedule(dynamic, 1) 
    for (int i = 0; i < roop_size; ++i) {
      ostringstream command; 
      command << boost::format("%s %s %f %d") % this_run.Executable % this_run.Prefix % param_pairs[i].first % param_pairs[i].second;
      cout << command.str() << endl; 
      system(command.str().c_str()); 
      command.str("");
      command.clear(std::stringstream::goodbit);
    }


    exit(EXIT_SUCCESS); 
  } catch (std::bad_alloc& e) {
    cerr << e.what() << endl; 
    cerr << "new threw bad_alloc" << endl; 
    exit(10); 
  } catch (std::logic_error& e) {
    cerr << e.what() << endl; 
    exit(20); 
  } 
  return 0; 
}

