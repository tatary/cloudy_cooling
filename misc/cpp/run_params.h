#ifndef RUN_PARAM_H
#define RUN_PARAM_H

#include <fstream>
#include <vector>
#include "utils.h"

const int STRING_LEN = 256; 

class RunParams {
  public: 
    // model name
    char Executable[STRING_LEN]; 
    char Prefix[STRING_LEN]; 
    std::vector<int> iden_array; 
    std::vector<double> redshifts; 
    int n_grid; 
};

#endif

