#ifndef __UTILS_H__
#define __UTILS_H__

#include <cmath>
#include <cstdlib>
#include <sys/time.h>
#include <sys/times.h>
#include <unistd.h>

template <class T> 
inline T sq(const T& x) 
{
  return x*x; 
} 

#if 0
inline double sq(const double x) {
  return x * x; 
}

inline float sq(const float x) {
  return x*x; 
}

inline int sq(const int x) {
  return x*x; 
}

inline size_t sq(const size_t x) {
  return x*x; 
}

inline unsigned int sq(const unsigned int x) {
  return x*x; 
}

#endif

template <class T> 
inline T cube(const T& x) {
  return x*x*x; 
}
  
#if 0
inline float cube(const float x) {
  return x*x*x; 
}

inline int cube(const int x) {
  return x*x*x; 
}

inline size_t cube(const size_t x) {
  return x*x*x; 
}

inline unsigned int cube (const unsigned int x) {
  return x*x*x; 
}
#endif 

inline double gettimeofday_sec() 
{
  timeval tv; 
  gettimeofday(&tv, NULL); 
  return static_cast<double>(tv.tv_sec) +  static_cast<double>(tv.tv_usec)*1.e-6; 
}

#ifndef CLK_TCK
#define CLK_TCK sysconf(_SC_CLK_TCK)
#endif

inline double timing(const struct tms& from, const struct tms& to) 
{ 
  return (static_cast<double>(to.tms_utime + to.tms_stime) 
      - static_cast<double>(from.tms_utime + from.tms_stime))/static_cast<double>(CLK_TCK);
}

// replace #define MAX(a, b) macro
template<class T> inline T MAX(const T& a, const T& b) 
{
  return ( a > b ? a : b ); 
}

template<class T> inline T MIN(const T& a, const T& b) 
{
  return ( a < b ? a : b ); 
}
#endif // __UTILS_H__
