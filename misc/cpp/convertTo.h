#ifndef CONVERTTO_H
#define CONVERTTO_H
#include <string>
#include <sstream>

template<typename T>
inline T convertTo(const std::string& fromString, T& toT)
{
  std::istringstream(fromString) >> toT;
  return toT;
}

template<typename T>
inline std::string convertFrom(const T& fromT, std::string& toString)
{
  std::ostringstream tmp; 
  tmp << fromT; 
  toString = tmp.str(); 
  return toString; 
}

#endif
