#ifndef __READ_PARAMS_H__
#define __READ_PARAMS_H__

#include "run_params.h" 
#include <string>

void read_params(RunParams& this_run, const std::string& fname); 

#endif
