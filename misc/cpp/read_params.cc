#include <iostream>
#include <fstream>
#include <stdexcept>
#include <vector>
#include <string>
#include <map>
#include <cstring>

#include "read_params.h"
#include "run_params.h"
#include "split.h"
#include "convertTo.h"

using std::cout; 
using std::cin; 
using std::cerr; 
using std::endl; 
using std::ifstream; 
using std::domain_error; 
using std::vector; 
using std::string; 
using std::map; 

enum var_type {
  INT, 
  LONG, 
  DOUBLE, 
  STRING, 
  IVECTOR, 
  DVECTOR, 
  BOOL
}; 

class AddressAndType {
  public: 
    void *var_adr; 
    var_type type; 
}; 

void read_params(RunParams& this_run, const string& fname)
{
  map<string, AddressAndType> input_params; 
  map<string, int> param_count; 
  AddressAndType aat; 

  aat.var_adr = &this_run.Executable; 
  aat.type    = STRING; 
  input_params["Executable"] = aat; 
  param_count["Executable"] = 0; 

  aat.var_adr = &this_run.Prefix; 
  aat.type    = STRING; 
  input_params["Prefix"] = aat; 
  param_count["Prefix"] = 0; 

  aat.var_adr = &this_run.iden_array; 
  aat.type    = IVECTOR; 
  input_params["iden_array"] = aat; 
  param_count["iden_array"] = 0; 

  aat.var_adr = &this_run.redshifts; 
  aat.type    = DVECTOR; 
  input_params["redshifts"] = aat; 
  param_count["redshifts"] = 0; 

  // OK, we are ready. Let's read from a file. 
  ifstream in(fname.c_str());   
  if (!in.is_open()) {
    cerr << "parameter file: " << fname << endl; 
    throw domain_error("cannot be opened.\n"); 
  }

  unsigned int count = 0; 
  string line; 
  while (getline(in, line)) {
    //cout << line << endl; 
    vector<string> v; 
    split(line, back_inserter(v)); 
    if (v.size() < 2) 
      continue; 
    else if (v[0][0] == '%') 
      continue; 

    map<string, AddressAndType>::iterator param_iter = input_params.find(v[0]); 
    map<string, int>::iterator count_iter = param_count.find(v[0]); 

    if (param_iter == input_params.end()) {
      cerr << "Variable: " << v[0] << " "; 
      throw domain_error("only exists in the parameter file.\n"); 
    } else if (count_iter->second > 0) {
      cerr << "count is " << count_iter->second << endl; 
      cerr << "The parameter: " << v[0]  << " "; 
      throw domain_error("is doupleicated!\n"); 
    } else {
      count_iter->second ++; 
      switch (param_iter->second.type) {
        case INT:
          {
            int *ivar = static_cast<int *>(param_iter->second.var_adr); 
            *ivar = convertTo(v[1], *ivar); 
          }
          ++count; 
          break; 
        case LONG:
          {
            unsigned long long *lvar = static_cast<unsigned long long *>(param_iter->second.var_adr); 
            *lvar = convertTo(v[1], *lvar); 
          }
          ++count; 
          break; 
        case DOUBLE:
          {
            double *fvar = static_cast<double *>(param_iter->second.var_adr); 
            *fvar = convertTo(v[1], *fvar); 
          }
          ++count; 
          break; 
        case STRING:
          {
            char *svar = static_cast<char *>(param_iter->second.var_adr); 
            strcpy(svar, v[1].c_str()); 
          }
          ++count; 
          break; 
        case BOOL:
          {
            bool *bvar = static_cast<bool *>(param_iter->second.var_adr); 
            *bvar = convertTo(v[1], *bvar); 
          }
          ++count; 
          break; 
        case IVECTOR: 
          { 
            vector<int> *ivec = static_cast<vector<int> *>(param_iter->second.var_adr); 
            for (int i = 1; i < v.size(); ++i) {
              int itemp; 
              itemp = convertTo(v[i], itemp); 
              (*ivec).push_back(itemp); 
            }
          }
          ++count; 
          break; 
        case DVECTOR: 
          { 
            vector<double> *dvec = static_cast<vector<double> *>(param_iter->second.var_adr); 
            for (int i = 1; i < v.size(); ++i) {
              double dtemp; 
              dtemp = convertTo(v[i], dtemp); 
              (*dvec).push_back(dtemp); 
            }
          }
          ++count; 
          break; 
      }

    }
  }
  in.close(); 

    
  if (count < input_params.size()) {
    cerr << "count = " << count << " input_param.size() = " << input_params.size() << endl; 
    for (map<string, int>::const_iterator iter = param_count.begin(); iter != param_count.end(); ++iter) {
      if (iter->second == 0) 
        cerr << iter->first << endl; 
    }
    throw domain_error("Your parameter file is likely to miss above variables.\n"); 
  }

}
