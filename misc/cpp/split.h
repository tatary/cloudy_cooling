#ifndef SPLIT_H
#define SPLIT_H

#include <string>
#include <cctype>
#include <algorithm>

// true if the argument is whitespace, false otherwise
inline bool space(char c)
{
  return std::isspace(c);
}

// false if the argument is whitespace, true otherwise
inline bool not_space(char c)
{
  return !std::isspace(c);
}

template <class Out> 
inline void split(const std::string& str, Out os) {
  typedef std::string::const_iterator iter;
  iter i = str.begin();
  while (i != str.end()) {
    // ignore leading blanks
    i = std::find_if(i, str.end(), not_space);

    // find end of next word
    iter j = std::find_if(i, str.end(), space);

    // copy the characters in [i, j)
    if (i != str.end())
      *os++ = std::string(i, j);
    i = j;
  }
}
#endif
