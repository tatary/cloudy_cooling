#include "cddefines.h"
#include "cddrive.h"
#include "cooling.h"
#include "thermal.h"
#include "colden.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string> 
#include <sstream>
#include <iomanip>
#include <cmath>
#include <omp.h>

#define CHRDIN

const int N_OUT_LIST = 5; 
#ifndef CHRDIN
const int N_RAHMATI  = 7; 
#else 
const int N_RAHMATI = 12; 
#endif
const double CosmicRayLog_nH = -2;
const double REDSHIFT_MAX = 15.13; 
static double rahmati_interpolation(const double nH, const double zred, const double *Rahmati_redshift, 
    const double *Rahmati_N0, const double *Rahmati_Alpha1, const double *Rahmati_Alpha2, 
    const double *Rahmati_Beta, const double *Rahmati_Unimf); 
static void read_redshifts(const string& file_name, vector<double>& redshifts) {
  FILE *fl;  
  if (!(fl = fopen(file_name.c_str(), "r"))) { 
    cerr << file_name; 
    throw domain_error(" cannot be opened\n"); 
  } 
  double redshift; 
  while (fscanf(fl, "%lf", &redshift) != EOF) {
    redshifts.push_back(redshift); 
  }
  fclose(fl); 
}

static void write_bins(const string& file_name, vector<double>& dvector) 
{
  FILE *fl; 

  if (!(fl = fopen(file_name.c_str(), "w"))) { 
    cerr << file_name; 
    throw domain_error(" cannot be opened\n"); 
  }
  const int len = dvector.size(); 

  for (int i = 0; i < len; ++i) 
    fprintf(fl, "%le\n", dvector[i]); 
  fclose(fl); 
}

static void calc_density_bin(vector<double>& log_nHs) 
{
  for (int i = -6; i <= 6; ++i) 
    log_nHs.push_back((double) i); 
}



static void calc_temperature_bin(vector<double>& log_Ts) 
{
  double log_Tmin = 1; 
  double log_Tmax = 9; 
  double dlog_T   = 0.25; 

  double log_T = log_Tmin; 
  while (log_T < 1.001*log_Tmax) {
    log_Ts.push_back(log_T); 
    log_T += dlog_T; 
  }
}

// iout = 0 log10(CoolingRate/nH^2)
// iout = 1 log10(HeatingRate/nH^2)
// iout = 2 specific internal energy in cgs 
// iout = 3 mean molecular weight 
// iout = 4 electron density 
#if 0 
#define OUTPUT_TABLE(imet, iden, itemp, iout) (*(output_table + iout + \
    N_OUT_LIST * ( (itemp) + ntemp * ( (iden) + nden * (imet) ) ) ))

#endif
#define OUTPUT_TABLE(itemp, iout) (*(output_table + iout + N_OUT_LIST * (itemp))) 
inline double fraction(const double n, const double n0, const double unimf, 
    const double alpha1, const double alpha2, const double beta) 
{ 
  return unimf*std::pow(1.0 + pow((n/n0), beta), alpha1) + (1.0 - unimf)*std::pow(1.0 + n/n0, alpha2); 
}

inline double linear_interporate(const double x, const double a, 
    const double b, const double fa, const double fb) 
{
  const double grad = (fb - fa) / (b - a); 
  const double intercept = fa - grad*a; 
  return intercept + grad*x; 
}

inline double linear_interporate_in_log(const double x, const double a, 
    const double b, const double fa, const double fb) 
{
  const double logfa = std::log10(fa); 
  const double logfb = std::log10(fb);

  const double grad = (logfb - logfa) / (b - a); 
  const double intercept = logfa - grad*x; 
  return std::pow(1.e1, grad*x + intercept); 
  
}

int main(int argc, char **argv) 
{ 
  exit_type exit_status = ES_SUCCESS; 
  DEBUG_ENTRY( "main()" );
  try {

    if (argc < 4) {
      cerr << "the number of arguments is " << argc << endl; 
      throw domain_error("You have to give the model name, redshift, and density index\n");  
    }
    /*
    const int N_MET = 2; 
    double metals[N_MET] = {-10, 0}; 
    */
    const int N_MET = 8; 
    double metals[N_MET] = {-10, -3, -2, -1, -0.5, 0, 0.5, 1}; 
		char chLine[100], chfile[256]; 
    vector<double> redshifts; 
    vector<double> log_nHs; 
    vector<double> log_Ts; 
#ifndef CHRDIN // Original Rahmati et al values 
    const double Rahmati_redshift[N_RAHMATI] = {0    , 1    , 2    , 3    , 4    , 5    , 15}; 
    const double Rahmati_N0[N_RAHMATI]       = {-2.56, -2.29, -2.06, -2.13, -2.23, -2.35, -2.35}; 
    const double Rahmati_Alpha1[N_RAHMATI]   = {-1.86, -2.94, -2.22, -1.99, -2.05, -2.63, -2.63}; 
    const double Rahmati_Alpha2[N_RAHMATI]   = {-0.51, -0.90, -1.09, -0.88, -0.75, -0.57, -0.57}; 
    const double Rahmati_Beta[N_RAHMATI]     = {2.83 , 1.21 , 1.75 , 1.72 , 1.93 , 1.77 , 1.77}; 
    const double Rahmati_Unimf[N_RAHMATI]    = {0.99 , 0.97 , 0.97 , 0.96 , 0.98 , 0.99 , 0.99}; 
#else // updated with Chardin et al. 2017  
    const double Rahmati_redshift[N_RAHMATI] = {0    , 1    , 2    , 3        , 4        , 5        , 6        , 7        , 8        , 9        , 10       , 15}; 
    const double Rahmati_N0[N_RAHMATI]       = {-2.56, -2.29, -2.06, -2.0440  , -2.0294  , -1.9838  , -2.1577  , -2.5754  , -2.4008  , -2.3374  , -2.3285  , -2.3285}; 
    const double Rahmati_Alpha1[N_RAHMATI]   = {-1.86, -2.94, -2.22, -1.115653, -0.950010, -1.294665, -0.941372, -0.866887, -0.742237, -0.642645, -0.386421, -0.386421}; 
    const double Rahmati_Alpha2[N_RAHMATI]   = {-0.51, -0.90, -1.09, -1.648170, -1.503310, -1.602099, -1.507124, -1.272957, -1.397100, -1.212105, -0.857335, -0.857335}; 
    const double Rahmati_Beta[N_RAHMATI]     = {2.83 , 1.21 , 1.75 ,  5.324900, 5.873852 , 5.056422 , 6.109438 , 7.077863 , 7.119987 , 9.988387 , 12.94204 , 12.94204}; 
    const double Rahmati_Unimf[N_RAHMATI]    = {0.99 , 0.97 , 0.97 , 0.98183  , 0.98469  , 0.97564  , 0.97117  , 0.95911  , 0.95879  , 0.97142  , 0.99429  , 0.99429}; 
#endif

    vector<double> metallicities(metals, metals + N_MET);  
    write_bins("log_metallicity.txt", metallicities); 

    calc_density_bin(log_nHs); 
    write_bins("log_nH.txt", log_nHs); 

    calc_temperature_bin(log_Ts); 
    write_bins("log_temp.txt", log_Ts); 

    //read_redshifts("redshifts.txt", redshifts); 

    //const int nzred = redshifts.size(); 
    const int nmet  = metallicities.size(); 
    const int nden  = log_nHs.size(); 
    const int ntemp = log_Ts.size(); 

    const double redshift = strtod(argv[2], NULL); 
    const int iden = atoi(argv[3]); 

    cout << "redshift = " << redshift << endl; 
    cout << "iden = " << iden << endl; 

   
    for (int imet = 0; imet < nmet; ++imet) {
      char ioOUT[128]; 
      FILE *ioFILE; 
      if (redshift <= REDSHIFT_MAX) {
        sprintf(ioOUT, "ascii/%s_z%2.2d.%2.2d_Z%d_nH%2.2d.log", 
            argv[1], (int)redshift, (int)(100*(redshift - (int)redshift)), imet, iden);  
        sprintf(chfile, "ascii/%s_z%2.2d.%2.2d_Z%d_nH%2.2d.txt", 
            argv[1], (int)redshift, (int)(100*(redshift - (int)redshift)), imet, iden);  
      } else { 
        sprintf(ioOUT, "ascii/%s_coronal_Z%d_nH%2.2d.log", 
            argv[1], imet, iden);  
        sprintf(chfile, "ascii/%s_coronal_Z%d_nH%2.2d.txt", 
            argv[1], imet, iden);  
      }
      if (!(ioFILE = fopen(chfile, "w"))) {
        cerr << chfile; 
        throw domain_error(" cannot be opened.\n"); 
      } 
      ostringstream fname; 
      if (redshift <= REDSHIFT_MAX) { 
        fname << "output/" << argv[1] << "_z" << setfill('0') 
          << setw(2) << (int) redshift << "." 
          << setfill('0') << setw(2) 
          << (int) (100 * (redshift- (int) redshift)) 
          << "_Z" << setfill('0') << setw(2) << imet 
          << "_nH" << setfill('0') << setw(2) << iden
          << ".dat"; 
      } else {
        // collisional equilibrium 
        fname << "output/" << argv[1] << "_CIE" 
          << "_Z" << setfill('0') << setw(2) << imet 
          << "_nH" << setfill('0') << setw(2) << iden 
          << ".dat"; 
      }
      
      ofstream out(fname.str().c_str(), ios::binary); 

      // table for output data 
      double *output_table = new double[ntemp*N_OUT_LIST]; 
 
      // table for output data 

      cout << "log_nH = " << log_nHs[iden] << " Z = " << metallicities[imet] << endl;  
      for (int itemp = 0; itemp < ntemp; ++itemp) {
        // initialize the code for this run  
        cdInit();
        // we do not want to generate any output 
        cdOutput( ioOUT );
        cdTalk(false);
        int nleft = 0; 
        // radiation field 
        if (redshift <= REDSHIFT_MAX) { 
          sprintf(chLine, "CMB redshfit %lf", redshift); 
          nleft = cdRead(chLine);   

          // UVB 
          // Rahmati et al's factor 
          double redshift_dummy = redshift; 
          /*
          if (log_nHs[iden] >= 3) 
            redshift_dummy = 0; 
            */
          double factor = rahmati_interpolation(std::pow(1.e1, log_nHs[iden]), redshift_dummy, Rahmati_redshift, 
              Rahmati_N0, Rahmati_Alpha1, Rahmati_Alpha2, Rahmati_Beta, Rahmati_Unimf); 
          if (factor <= 0) 
            factor = -30; 
          sprintf(chLine, "table HM12 redshift %lf factor=%lf", redshift_dummy, log10(factor)); 
          cout << chLine << endl; 
          nleft = cdRead(chLine); 

        } else if (log_nHs[iden] >= CosmicRayLog_nH) {
          sprintf(chLine, "table HM12 redshift %lf factor=%lf", 0.0, -30.0); 
          cout << chLine << endl; 
          nleft = cdRead(chLine); 
        }
        double CR_MAX = metallicities[imet]; 
        CR_MAX = 0.0; 
        //CR_MAX = -30 + (0. - (-30.))/(1 - (-10)) * (metallicities[imet] - (-10)); 
        CR_MAX = (CR_MAX > 0 ? 0.0 : CR_MAX); 
        if (log_nHs[iden] >= CosmicRayLog_nH) {
          if (log_nHs[iden] >= 1) {
            sprintf(chLine, "cosmic rays background %lf", CR_MAX);  
          } else {
            double log_fact = CR_MAX + -1.0 + (log_nHs[iden] - CosmicRayLog_nH) * ((0.0 - (-1.0))/(1.0 - CosmicRayLog_nH)); 
            if (log_fact > 0) {
              cout << "SOMETHING IS WRONG!!" << endl; 
            }
            if (log_fact < -30) 
              log_fact = -30; 
            sprintf(chLine, "cosmic rays background %lf", log_fact);  
          }
          cout << chLine << endl; 
          nleft = cdRead(chLine); 

          sprintf(chLine,"constant temperature %lf",log_Ts[itemp]);
          nleft = cdRead(chLine); 
        } else if (redshift <= REDSHIFT_MAX) {
          /*
          sprintf(chLine, "cosmic rays background -30.0");  
          cout << "cosmic rays background -30.0" << endl; 
          nleft = cdRead(chLine);
          */

          sprintf(chLine, "no H2 molecules");  
          nleft = cdRead(chLine);

          sprintf(chLine, "no CO molecules");  
          nleft = cdRead(chLine);


          sprintf(chLine,"constant temperature %lf",log_Ts[itemp]);
          nleft = cdRead(chLine); 
        } else {
#if 0
          sprintf(chLine, "cosmic rays background %lf", -30.0);  
          cout << "cosmic rays background -30.0" << endl; 
          nleft = cdRead(chLine);
#endif
          sprintf(chLine, "no H2 molecules");  
          nleft = cdRead(chLine);

          sprintf(chLine, "no CO molecules");  
          nleft = cdRead(chLine);

          sprintf(chLine,"coronal %lf",log_Ts[itemp]);
          nleft = cdRead(chLine); 
        }

        // do only one zone
        nleft = cdRead("stop zone 1"); 

        sprintf(chLine, "hden %lf log", log_nHs[iden]); 
        nleft = cdRead(chLine); 

        sprintf(chLine,"metals %lf log", metallicities[imet]); 
        nleft = cdRead(chLine); 

        /* other commands */
        sprintf(chLine,"iterate to convergence");
        nleft = cdRead(chLine);

        sprintf(chLine,"double");
        nleft = cdRead(chLine);

        sprintf(chLine,"stop temperature 10 linear");
        nleft = cdRead(chLine);

        sprintf(chLine,"print last");
        nleft = cdRead(chLine);

        sprintf(chLine,"print short");
        nleft = cdRead(chLine);

        /* call the code */
        int lgBAD = cdDrive(); 

        /* get cooling for last zone */
        double cool_rate = cdCooling_last();

        /* get heating for last zone */
        double heat_rate = cdHeating_last();

        cool_rate /= std::pow(10.0, 2*log_nHs[iden]); 
        heat_rate /= std::pow(10.0, 2*log_nHs[iden]); 
        const double wmean = colden.wmas/SDIV(colden.TotMassColl); 
        cool_rate = std::log10(cool_rate); 
        heat_rate = std::log10(heat_rate); 
        const double log_uene = std::log10(
            1.5*BOLTZMANN*std::pow(1.e1, log_Ts[itemp])/(wmean*PROTON_MASS) 
            ); 
        const double log_ne = std::log10(cdEDEN_last()); 
        OUTPUT_TABLE(itemp, 0) = cool_rate; 
        OUTPUT_TABLE(itemp, 1) = heat_rate; 
        OUTPUT_TABLE(itemp, 2) = log_uene;  
        OUTPUT_TABLE(itemp, 3) = wmean; 
        OUTPUT_TABLE(itemp, 4) = log_ne; 
        if (itemp == 0) {
          if (redshift <= REDSHIFT_MAX) 
            fprintf(ioFILE, "# z = %e\n", redshift); 
          else 
            fprintf(ioFILE, "# CIE\n"); 
          fprintf(ioFILE, "# log(Z/Zsun) = %e\n", metallicities[imet]); 
          fprintf(ioFILE, "# log(nH) = %e\n", log_nHs[iden]); 
        }

        fprintf(ioFILE, "%.3f %.3f %.3f %.3f %.3f %.3f\n", 
            log_Ts[itemp], cool_rate, heat_rate, log_uene, wmean, log_ne); 
        if (lgBAD) {
          cerr << "problems!" << endl; 
          cerr << "log_nH = " << log_nHs[iden] << " log_Z = " 
            << metallicities[imet] <<  " log_T = " << log_Ts[itemp] << endl;  
          throw domain_error("PROBLEMS\n"); 
        }
      }
      
      fclose(ioFILE); 
      out.write((char *) output_table, sizeof(double)*nmet*nden*ntemp*N_OUT_LIST ); 
      out.close(); 
      delete [] output_table; 
    }


  } catch( bad_alloc ) {
		fprintf( ioQQQ, " DISASTER - A memory allocation has failed. Most likely your computer "
			 "ran out of memory.\n Try monitoring the memory use of your run. Bailing out...\n" );
		exit_status = ES_BAD_ALLOC;
	} catch( out_of_range& e ) {
		fprintf( ioQQQ, " DISASTER - An out_of_range exception was caught, what() = %s. Bailing out...\n",
			 e.what() );
		exit_status = ES_OUT_OF_RANGE;
	} catch( bad_assert& e ) {
		MyAssert( e.file(), e.line() , e.comment() );
		exit_status = ES_BAD_ASSERT;
  }
#ifdef CATCH_SIGNAL
	catch( bad_signal& e ) {
		if( ioQQQ != NULL ) {
			if( e.sig() == SIGINT || e.sig() == SIGQUIT ) {
				fprintf( ioQQQ, " User interrupt request. Bailing out...\n" );
				exit_status = ES_USER_INTERRUPT;
			} else if( e.sig() == SIGTERM ) {
				fprintf( ioQQQ, " Termination request. Bailing out...\n" );
				exit_status = ES_TERMINATION_REQUEST;
			} else if( e.sig() == SIGILL ) {
				fprintf( ioQQQ, " DISASTER - An illegal instruction was found. Bailing out...\n" );
				exit_status = ES_ILLEGAL_INSTRUCTION;
			} else if( e.sig() == SIGFPE ) {
				fprintf( ioQQQ, " DISASTER - A floating point exception occurred. Bailing out...\n" );
				exit_status = ES_FP_EXCEPTION;
			} else if( e.sig() == SIGSEGV ) {
				fprintf( ioQQQ, " DISASTER - A segmentation violation occurred. Bailing out...\n" );
				exit_status = ES_SEGFAULT;
			}
#			ifdef SIGBUS
			else if( e.sig() == SIGBUS ) {
				fprintf( ioQQQ, " DISASTER - A bus error occurred. Bailing out...\n" );
				exit_status = ES_BUS_ERROR;
			}
#			endif
			else {
				fprintf( ioQQQ, " DISASTER - A signal %d was caught. Bailing out...\n", e.sig() );
				exit_status = ES_UNKNOWN_SIGNAL;
			}
		}
	}
#endif 
  catch( cloudy_exit& e ) {
		if( ioQQQ != NULL ) {
			ostringstream oss;
			oss << " [Stop in " << e.routine();
			oss << " at " << e.file() << ":" << e.line();
			if( e.exit_status() == 0 )
				oss << ", Cloudy exited OK]";
			else
				oss << ", something went wrong]";
			fprintf( ioQQQ, "%s\n", oss.str().c_str() );
		}
		exit_status = e.exit_status();
	}
  catch( std::exception& e ) {
		fprintf( ioQQQ, " DISASTER - An unknown exception was caught, what() = %s. Bailing out...\n",
			 e.what() );
		exit_status = ES_UNKNOWN_EXCEPTION;
	}
	// generic catch-all in case we forget any specific exception above... so this MUST be the last one.
	catch( ... ) {
		fprintf( ioQQQ, " DISASTER - An unknown exception was caught. Bailing out...\n" );
		exit_status = ES_UNKNOWN_EXCEPTION;
	}

	cdPrepareExit(exit_status);

	return exit_status;

}

static double rahmati_interpolation(const double nH, const double zred, const double *Rahmati_redshift, 
    const double *Rahmati_N0, const double *Rahmati_Alpha1, const double *Rahmati_Alpha2, 
    const double *Rahmati_Beta, const double *Rahmati_Unimf)
{
#ifndef CHRDIN
  double zmax = Rahmati_redshift[5]; 
  int nmax = 5; 
#else 
  double zmax = Rahmati_redshift[10]; 
  int nmax = 10; 
#endif
  if (zred <= 0) {
    return fraction(nH, std::pow(1.e1, Rahmati_N0[0]), Rahmati_Unimf[0], Rahmati_Alpha1[0], 
        Rahmati_Alpha2[0], Rahmati_Beta[0]); 
  } else if (zred >= zmax) {
    return fraction(nH, std::pow(1.e1, Rahmati_N0[nmax]), Rahmati_Unimf[nmax], Rahmati_Alpha1[nmax], 
        Rahmati_Alpha2[nmax], Rahmati_Beta[nmax]); 
  } else {
    int counter = 0; 
    for (int i = 0; i < N_RAHMATI; ++i) {
      if (zred < Rahmati_redshift[i]) {
        counter = i; 
        break; 
      }
    }

    cout << "zred = " << zred << " Rahmati_redshift = " << Rahmati_redshift[counter] << endl; 
    double logn0_interpol = linear_interporate(zred, Rahmati_redshift[counter-1], Rahmati_redshift[counter], 
        Rahmati_N0[counter-1], Rahmati_N0[counter]); 
    double alpha1_interpol = linear_interporate(zred, Rahmati_redshift[counter-1], Rahmati_redshift[counter], 
        Rahmati_Alpha1[counter-1], Rahmati_Alpha1[counter]); 
    double alpha2_interpol = linear_interporate(zred, Rahmati_redshift[counter-1], Rahmati_redshift[counter], 
        Rahmati_Alpha2[counter-1], Rahmati_Alpha2[counter]); 
    double beta_interpol   = linear_interporate(zred, Rahmati_redshift[counter-1], Rahmati_redshift[counter], 
        Rahmati_Beta[counter-1], Rahmati_Beta[counter]); 
    double unimf_interpol  = linear_interporate(zred, Rahmati_redshift[counter-1], Rahmati_redshift[counter], 
        Rahmati_Unimf[counter-1], Rahmati_Unimf[counter]); 
    return fraction(nH, std::pow(1.e1, logn0_interpol), unimf_interpol, alpha1_interpol, alpha2_interpol, beta_interpol); 
  }
}
