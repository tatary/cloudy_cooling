#!/usr/bin/ruby

Cloudy = "../c13.05/source/cloudy.exe"
Prefix = "rahmati"
Redshifts = [0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.5, 5.0, 5.5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
#-6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6
Densities = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

Redshifts.each do |var_redshift|
  Densities.each do |var_iden|
    log_file = "log" + var_redshift.to_s + "_" + var_iden.to_s
    command  =  Cloudy + " " + Prefix + " " +  var_redshift.to_s + " " + var_iden.to_s + " > " + log_file  + " 2>&1 &"
    puts command 
    result = system(command)
  end
end
