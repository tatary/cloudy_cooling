# z = 1.500000e+00
# log(Z/Zsun) = 5.000000e-01
# log(nH) = -2.000000e+00
1.000 -26.374 -22.530 9.089 1.008 -2.454
1.250 -25.593 -22.559 9.354 0.974 -2.396
1.500 -24.781 -22.593 9.620 0.939 -2.339
1.750 -24.267 -22.634 9.887 0.904 -2.285
2.000 -23.914 -22.683 10.153 0.870 -2.234
2.250 -23.613 -22.741 10.420 0.836 -2.187
2.500 -23.385 -22.809 10.687 0.805 -2.144
2.750 -23.241 -22.886 10.952 0.777 -2.105
3.000 -23.152 -22.975 11.217 0.752 -2.072
3.250 -23.100 -23.075 11.479 0.730 -2.043
3.500 -23.043 -23.184 11.740 0.713 -2.020
3.750 -22.909 -23.302 11.999 0.699 -2.002
4.000 -22.610 -23.426 12.255 0.688 -1.987
4.250 -21.993 -23.590 12.512 0.677 -1.974
4.500 -21.621 -24.016 12.771 0.664 -1.957
4.750 -21.167 -24.211 13.023 0.660 -1.951
5.000 -20.803 -24.485 13.288 0.638 -1.922
5.250 -20.790 -24.582 13.541 0.634 -1.917
5.500 -20.950 -24.617 13.792 0.632 -1.915
5.750 -21.236 -24.634 14.042 0.632 -1.914
6.000 -21.332 -24.640 14.293 0.631 -1.914
6.250 -21.478 -24.642 14.543 0.630 -1.913
6.500 -21.914 -24.644 14.794 0.630 -1.912
6.750 -22.075 -24.645 15.044 0.629 -1.911
7.000 -22.090 -24.645 15.294 0.629 -1.911
7.250 -22.314 -24.646 15.544 0.629 -1.911
7.500 -22.418 -24.646 15.794 0.629 -1.911
7.750 -22.378 -24.647 16.044 0.629 -1.911
8.000 -22.304 -24.647 16.294 0.629 -1.911
8.250 -22.222 -24.647 16.544 0.629 -1.910
8.500 -22.115 -24.647 16.794 0.629 -1.910
8.750 -21.986 -24.647 17.044 0.629 -1.910
9.000 -21.839 -24.647 17.294 0.629 -1.910
